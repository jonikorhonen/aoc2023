package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main () {
    solve()
}

var mapping = map[string]string {
    "one"   : "1",
    "two"   : "2",
    "three" : "3",
    "four"  : "4",
    "five"  : "5",
    "six"   : "6",
    "seven" : "7",
    "eight" : "8",
    "nine"  : "9",
}

func solve () {
    f, err := os.Open("./01/input.txt")
    if err != nil {
        log.Fatal(err)
    }

    defer f.Close()

    scanner := bufio.NewScanner(f)
    var result int64 = 0
    for scanner.Scan() {
        line := scanner.Text()

        pair := ""
        pair += min_word_numeric_match(line)
        pair += max_word_numeric_match(line)

        if num, err := strconv.ParseInt(pair, 10, 32); err == nil {
            result += num
        }
    }
    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    fmt.Printf("Result: %d\n", result)
}

func min_word_numeric_match(line string) string {
    match := ""

    // Check if numeric word matches anything, with index
    min := len(line)
    for k, v := range mapping {
        cur := strings.Index(line, k)
        if (cur != -1 && cur < min) {
            min = cur
            match = v
        }
    }

    // Check if actual number happens to be before
    runes := []rune(line)
    for i := 0; i < len(runes); i++ {
        if _, err := strconv.ParseInt(string(runes[i]), 10, 32); err == nil && i < min {
            match = string(runes[i])
            break
        }
    }

    return match
}

func max_word_numeric_match(line string) string {
    match := ""

    // Check if numeric word matches anything, with index
    max := -1
    for k, v := range mapping {
        cur := strings.LastIndex(line, k)
        if (cur != -1 && cur > max) {
            max = cur
            match = v
        }
    }

    // Check if actual number happens to be after
    runes := []rune(line)
    last := len(runes)-1
    for i := 0; i < len(runes); i++ {
        if _, err := strconv.ParseInt(string(runes[last-i]), 10, 32); err == nil && last-i > max {
            match = string(runes[last-i])
            break
        }
    }

    return match
}
