# Advent of Code 2023

[Advent of Code 2023](https://adventofcode.com/2023) solutions in Golang.

## Info

Daily challenges in numbered directories with `main.go` containing code and optional `input` file containing challenge input

## Running

```
go run {day}/main.go
```

