package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func main() {
    solve()
}

type Dices struct {
    Red     int64
    Green   int64
    Blue    int64
}

func solve() {
    f, err := os.Open("./02/input.txt")
    if err != nil {
        log.Fatal(err)
    }
    defer f.Close()

    limit := Dices{12, 13, 14}
    result := 0
    result2 := 0

    scanner := bufio.NewScanner(f)
    for scanner.Scan() {
        line := scanner.Text()
        parts := strings.SplitN(line, ":", 2)

        dices := combine(parse_dices(parts[1]))
        if dices.fits_to(limit) {
            game_id, _ := parse_id(parts[0])
            result += int(game_id)
        }

        result2 += int(dices.power())
    }


    if err := scanner.Err(); err != nil {
        log.Fatal(err)
    }

    fmt.Printf("Result:  %d\nResult2: %d\n", result, result2)
}

func parse_id(line string) (int64, error) {
    re := regexp.MustCompile(`\d+$`)
    return strconv.ParseInt(string(re.Find([]byte(line))), 10, 32)
}

func parse_dices(line string) []Dices {
    var result []Dices
    // Group of dices
    parts := strings.Split(line, ";")
    for _, palm := range parts {
        to_add := Dices{0, 0, 0}
        // Dices of particular color
        parts := strings.Split(palm, ",")
        for _, d := range parts {
            d = strings.TrimSpace(d)
            // Pair of count and color
            parts := strings.SplitN(d, " ", 2)
            if num, err := strconv.ParseInt(parts[0], 10, 32); err == nil {
                if parts[1] == "red" {
                    to_add.Red = num
                }
                if parts[1] == "green" {
                    to_add.Green = num
                }
                if parts[1] == "blue" {
                    to_add.Blue = num
                }
            }
        }
        result = append(result, to_add)
    }
    return result
}


func combine(groups []Dices) Dices {
    result := Dices{0, 0, 0}
    for _, g := range groups {
        if g.Red > result.Red {
            result.Red = g.Red
        }
        if g.Green > result.Green {
            result.Green = g.Green
        }
        if g.Blue > result.Blue {
            result.Blue = g.Blue
        }
    }
    return result;
}

func (self *Dices) fits_to(to_compare Dices) bool {
    if self.Red > to_compare.Red {
        return false
    }
    if self.Green > to_compare.Green {
        return false
    }
    if self.Blue > to_compare.Blue {
        return false
    }

    return true;
}

func (self *Dices) power() int64 {
    return value_not_zero_or(self.Red, 1) * value_not_zero_or(self.Green, 1) * value_not_zero_or(self.Blue, 1)
}

func value_not_zero_or(value int64, or int64) int64 {
    if value == 0 {
        return or
    }
    return value
}
